# ngtool

## 介绍
ng-form动态表单生态之后端校验、pdf生成等工具集
 
 
## 简介
配套ng-form前端动态表单的后端服务，ng-form前端组件一览：

**element-ui版本地址:**
 https://gitee.com/jjxliu306/ng-form-element

**iview版本地址:**
 https://gitee.com/jjxliu306/ng-form-iview

**element-plus版本地址:**
https://gitee.com/jjxliu306/ng-form-elementplus
 
## 安装
```
<dependency>
	<groupId>io.github.jjxliu306</groupId>
	<artifactId>ng-form-core</artifactId>
	<version>1.0.1</version>
</dependency>
```

## 使用示例

```
package com.ng;

import java.io.IOException;

import org.apache.commons.io.IOUtils;

import com.alibaba.fastjson.JSON;
import com.ng.form.validator.NgFormValidator;
import com.ng.form.validator.entity.FormValidator;

public class TestValidator {

	public void test1() throws IOException {
		
		String template = IOUtils.toString(getClass().getResource("/template.json") , "UTF-8");
		String value = IOUtils.toString(getClass().getResource("/value.json") , "UTF-8");
		
		NgFormValidator validator = new NgFormValidator();
		FormValidator fv = validator.validator(template, value);
		
		System.out.println(JSON.toJSONString(fv));
		
	}
	
	
	public static void main(String[] args) throws IOException {
		TestValidator test = new TestValidator();
		test.test1();
	}
}

```
**其中template.json如下**
```
{
	"list": [
		{
			"type": "input",
			"label": "姓名",
			"options": {
				"type": "text",
				"width": "100%",
				"defaultValue": "",
				"placeholder": "请输入",
				"clearable": false,
				"maxLength": 0,
				"prepend": "",
				"append": "",
				"tooptip": "",
				"hidden": false,
				"disabled": false,
				"dynamicHide": false,
				"dynamicHideValue": ""
			},
			"model": "name",
			"key": "input_1628755200021",
			"rules": [
				{
					"required": true,
					"message": "姓名不能为空",
					"trigger": [
						"blur"
					]
				}
			]
		},
		{
			"type": "textarea",
			"label": "地址",
			"options": {
				"width": "100%",
				"maxLength": 0,
				"defaultValue": "",
				"rows": 4,
				"clearable": false,
				"tooptip": "",
				"hidden": false,
				"disabled": false,
				"placeholder": "请输入",
				"dynamicHide": false,
				"dynamicHideValue": ""
			},
			"model": "address",
			"key": "textarea_1628755200021",
			"rules": [
				{
					"required": true,
					"message": "地址不能为空",
					"trigger": [
						"blur"
					]
				}
			]
		},
		{
			"type": "radio",
			"label": "性别",
			"options": {
				"disabled": false,
				"hidden": false,
				"defaultValue": "1",
				"dynamic": 0,
				"tooptip": "",
				"remoteFunc": "",
				"dataPath": "",
				"remoteValue": "",
				"remoteLabel": "",
				"linkage": false,
				"options": [
					{
						"value": "1",
						"label": "男"
					},
					{
						"value": "2",
						"label": "女"
					}
				],
				"dynamicHide": false,
				"dynamicHideValue": ""
			},
			"model": "sex",
			"key": "radio_1628755200021",
			"rules": [
				{
					"required": false,
					"message": "性别不能为空",
					"trigger": [
						"change",
						"blur"
					]
				}
			]
		},
		{
			"type": "textarea",
			"label": "男性输入",
			"options": {
				"width": "100%",
				"maxLength": 0,
				"defaultValue": "",
				"rows": 4,
				"clearable": false,
				"tooptip": "",
				"hidden": false,
				"disabled": false,
				"placeholder": "请输入",
				"dynamicHide": false,
				"dynamicHideValue": ""
			},
			"model": "nanInput",
			"key": "textarea_1628755202176",
			"rules": [
				{
					"required": false,
					"message": "男性输入不能为空",
					"trigger": [
						"blur"
					]
				},
				{
					"vtype": 2,
					"pattern": "",
					"script": "if($.sex == 1){\nreturn $.nanInput\n}else{\nreturn true\n}",
					"message": "男性输入不能为空",
					"required": false,
					"trigger": [
						"change",
						"blur"
					]
				}
			]
		}
	],
	"config": {
		"labelPosition": "left",
		"labelWidth": 100,
		"size": "mini",
		"outputHidden": true,
		"hideRequiredMark": true,
		"customStyle": ""
	}
}
```

**value.json如下**
```
{"name":"小米" , "address":"" , "sex":1 }
```

**最终输出结果如下**
```

{
    "failField":[
        {
            "fieldName":"地址",
            "message":"地址不能为空",
            "model":"address",
            "type":"textarea"
        },
        {
            "fieldName":"男性输入",
            "message":"男性输入不能为空",
            "model":"nanInput",
            "type":"textarea"
        }
    ],
    "result":false
}
```


## api说明
**在线api地址：** 
https://apidoc.gitee.com/jjxliu306/ngtool/

## 交流
点击链接加入qq群聊，可以直接提问及反馈bug 【交流群：203205848】 <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=vNN2AMPxXjojpwYu66DOGNtL2dFYh6Q-&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="vue-form-design" title="vue-form-design"></a>
